import api, {route, fetch, storage} from "@forge/api";
import ForgeUI, {
    render,
    Fragment,
    Text,
    Macro,
    useProductContext,
    useState,
    Tag,
    TagGroup,
    ButtonSet,Strong,StatusLozenge, Badge,
    Button, Tabs, Tab, ModalDialog, ContentBylineItem
} from '@forge/ui';
import axios from "axios";

const _ = require('lodash');
const punctuator = require('punctuator');
const {convert} = require('html-to-text');
const baseURL = "https://nlapi.expert.ai";
const language = "en";
const fetchCommentsForContent = async (contentId) => {
    const res = await api
        .asUser()
        .requestConfluence(route`/rest/api/content/${contentId}/child/comment`);

    const data = await res.json();
    return data.results;
};
const fetchContent = async (contentId) => {
    const res = await api
        .asApp()
        .requestConfluence(route`/rest/api/content/${contentId}?expand=body.view`);

    const data = await res.json();
    return data;
};


const App = () => {
    const context = useProductContext();
    const [comments] = useState(async () => await fetchCommentsForContent(context.contentId));
    const [content] = useState(async () => await fetchContent(context.contentId));
    const [transcript, setTranscript] = useState("");
    const [token, setToken] = useState('');
    const [keyNotes, setKeyNotes] = useState([]);
    const [summary, setSummary] = useState([]);
    const [topics, setTopics] = useState([]);
    const [quiz, setQuiz] = useState([]);
    const [formState, setFormState] = useState(undefined);
    const [overallSentiment, setOverallSentiment] = useState(undefined);
    const [isOpen, setOpen] = useState(true)

    if (!isOpen) {
        return null;
    }

    const getToken = async () => {

        let obj = await storage.get("key").then(res => res ? JSON.parse(res) : null);
        const time_spend = (new Date().getTime() - obj?.timestamp) / 1000;
        if (obj === null || time_spend > 3600) {
            const response = await axios.post(`https://developer.expert.ai/oauth2/token/`, {
                "username": "sagarbansal099@gmail.com",
                "password": "h2kvK9hNHJVj!E2"
            }, {
                headers: {
                    "Content-Type": "application/json"
                }
            });
            if (response.status === 200) {
                const t = "Bearer " + response.data
                await setToken(t);
                const object = {token: t, timestamp: new Date().getTime()}
                await storage.set("key", JSON.stringify(object));
                return t;
            }
        } else {
            await setToken(obj.token);
            return obj.token;
        }
    }

    const getKeyElements = async (transcript, token) => {
        const payload = {
            document: {
                text: transcript
            }
        }
        const headers = {
            "accept": "application/json",
            "Authorization": token,
            "Content-Type": "application/json; charset=utf-8"
        }
        return axios.post(`${baseURL}/v2/analyze/standard/${language}/relevants`, payload, {headers: headers})
            .then(res => {
                return res.data['data'];
            }).catch(err => console.error(err));
    };

    const getSentiment = async (transcript, token) => {
        const payload = {
            document: {
                text: transcript
            }
        }
        const headers = {
            "accept": "application/json",
            "Authorization": token,
            "Content-Type": "application/json; charset=utf-8"
        }
        return axios.post(`${baseURL}/v2/analyze/standard/${language}/sentiment`, payload, {headers: headers})
            .then(async res => {
                return res.data['data']
            })
            .catch(er => console.error(er));
    }


    const htmlParser = (html) => {
        const options = {
            selectors: [
                {selector: 'p'},
                {selector: 'a', options: {ignoreHref: true, noAnchorUrl: true}},
                {selector: 'img', format: 'skip'},
                {selector: 'sup', format: 'skip'},
                {selector: 'sub', format: 'skip'}
            ]
        }
        return convert(html, options);
    }

    const startHandler = async () => {
        const htmlBody = content.body.view.value;
        console.log("triggered")
        let text = htmlParser(htmlBody);
        text = punctuator.punctuate(text);
        setTranscript(text);
        const t = await getToken();
        await getKeyElements(text, t).then(async data => {
            if (data) {
                let topicsArray = [];
                data['topics'].forEach(topic => {
                    topicsArray.push(topic.label);
                });
                setTopics(topicsArray.slice(0, 6));
                setSummary(data['mainSentences']);
                setKeyNotes(data['mainLemmas']);
                await getSentiment(text, t).then(async data => {
                    if (data) {
                        let sentiment = data.sentiment;
                        console.log(sentiment)
                        let sentimentsArray = sentiment.items;
                        await setOverallSentiment(sentiment.overall);

                        // let chartData = [
                        //     _.meanBy(_.filter(sentimentsArray, o => o.sentiment < -8), o => o.sentiment),
                        //     _.meanBy(_.filter(sentimentsArray, o => o.sentiment < 0 && o.sentiment >= -8), o => o.sentiment),
                        //     _.meanBy(_.filter(sentimentsArray, o => o.sentiment < 3 && o.sentiment >= 0), o => o.sentiment),
                        //     _.meanBy(_.filter(sentimentsArray, o => o.sentiment < 8 && o.sentiment >= 3), o => o.sentiment),
                        //     _.meanBy(_.filter(sentimentsArray, o => o.sentiment >= 8), o => o.sentiment)
                        // ]
                    }
                });
            }
        });
    }

    const getSummary = () => {
        summary.sort((a, b) => {
            if (a.start >= b.start) return 1;
            else return -1;
        });
        let text = '';
        for (let s in summary) {
            text += summary[s].value + ' '
        }
        return text;
    };


    return (
        <ModalDialog header="WordCubo" onClose={() => setOpen(false)} closeButtonText={'Close'} width={"large"}>
            <Fragment>
                <Text><Strong>Welcome to the WordCubo!</Strong></Text>
                <ButtonSet><Button text={'Start Analysis'} onClick={startHandler}/></ButtonSet>

                {summary.length > 0 ?
                    <Tabs>
                        <Tab label="Topics">
                            <Fragment>
                                <TagGroup>
                                    {topics.length > 0 ? topics.map(topic => (
                                            <Tag text={topic} color={"blue-light"}></Tag>))
                                        : ''}
                                </TagGroup>
                                {topics.length === 0 ? <Text>Click on Start Analysis to Get Started.</Text> : ''}
                            </Fragment>
                        </Tab>
                        <Tab label="Key Notes">
                            <Fragment>
                                <TagGroup>
                                    {keyNotes.length > 0 ? keyNotes.map(kn => (
                                            <Tag text={kn.value} color={"blue-light"}></Tag>))
                                        : ''}
                                </TagGroup>
                                {topics.length === 0 ? <Text>Click on Start Analysis to Get Started.</Text> : ''}
                            </Fragment>
                        </Tab>
                        <Tab label="Summary">

                            <Fragment>
                                {summary.length > 0 ?
                                    <Text>{getSummary()}</Text>
                                    : <Text>Click on Start Analysis to Get Started.</Text>}
                            </Fragment>

                        </Tab>
                        <Tab label="Sentiment">

                            <Fragment>
                                {overallSentiment !== undefined ?
                                    <Text>Overall Sentiment Score: <StatusLozenge text={overallSentiment} appearance="inprogress" /></Text>
                                    : <Text>Click on Start Analysis to Get Started.</Text>}
                                <Text><Badge text={'-100'} appearance={"removed"}></Badge> Most Negative and <Badge text={'+100'} appearance={"added"}></Badge>  Most Positive</Text>
                            </Fragment>

                        </Tab>
                    </Tabs>
                    : ''}

            </Fragment>
        </ModalDialog>

    );
};

export const run = render(
        // <Macro
        //     app={<App/>}
        // />
        <ContentBylineItem>
            <App/>
        </ContentBylineItem>
    );
