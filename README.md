# `WordCubo`

## Quick start

- Clone the repository. And cd into it.

- Build and deploy your app by running:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:
```
forge tunnel
```


## Inspiration
Throughout my career as a software developer and working with major IT consultants I’ve always thought to minimize the hassle, and thrive in an evolving world. In our day to the day development cycle, we use Confluence, to document all of our important stuff. But the problems start whenever we revisit to look at those documents, many times we find ourselves scrolling through pages trying to find a punchline or a specific part of a document. 
It can be time-consuming and mind-numbing to try and find a key topic in a document. (And this doesn’t end after one or two)​

#### Privacy Issues
Although plenty of such NLP solutions are available online and usage is extravagant, often with the limit of the data privacy we just can’t copy-paste the content online to get the relevant inference. WordCubo integrated with the confluence provides the safe option of analyzing the documents without any worry.

#### Study Material
The transition to online classes has, contrary to expectations, increased the average time spent for classes. To improve studying efficiency, we decided to tackle a major part of our long study times and comprehension ability: Zoom lectures. We thought it’d be nice if we could somehow condense lecture material without losing comprehension and possibly even increasing our understanding.

#### Legal Documents
In 2008 a study estimated that it would take 244 hours a year for the typical American internet user to read the privacy policies of all websites he or she visits – and that was before everyone carried smartphones with dozens of apps, before cloud services, and before smart home technologies.

## What it does
The core of the idea is to remove those hassles of finding that “the” information in the confluence document to give users quick and most relevant content.​

WordCubo for Confluence, a forge-integrated NLP tool to provide users with a seamless learning experience by giving a fully-fledged solution that aims to reduce the time and hassles we faced day-to-day and provide a timeless solution so as to make your learning efficient.

## How I built it
WordCubo is built on the Atlassian Forge with the integration of the Confluence APIs for the document extractor and Expert.AI for the NLP APIs. The front end of this app was created with the inbuilt UI-Kit for a cutting-edge user experience. Forge CLI provides continuous integration and automated testing, while Bitbucket for code management.

First, extract the body from the Confluence page by using the Confluence API and this HTML body of the page is feed into the HTML to Plain text parser which filters out the unwanted content such as hyperlinks and images and returns the plain text. Plain text gets further cleaned and prepared to be used in the NLP APIs. With the help of this cleaned data, API generates the keynotes, highlight key main sentences, and the main topic. Also did the sentimental analysis of the document using the sentiment API. All this information returned to the Forge App and finally to the user.
![Pipeline](https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/001/653/070/datas/original.png)


See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge.

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
